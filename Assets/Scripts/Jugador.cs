using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jugador : MonoBehaviour
{
    private Animator animator;
    private SpriteRenderer sprite;
    private Rigidbody2D rb;

    private bool gameover;

    void Start()
    {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        if(gameover == false)
        {
            Time.timeScale = 1f;
            animator.SetInteger("Estado", 0);
            rb.velocity = new Vector2(10, rb.velocity.y);

            if (Input.GetKeyDown(KeyCode.Space))
            {
                animator.SetInteger("Estado", 1);
                rb.velocity = new Vector2(10, 45);
            }
        }
        else
        {
            animator.SetInteger("Estado", 2);
            rb.velocity = new Vector2(0, rb.velocity.y);
            Time.timeScale = 0f;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            gameover = true;
        }
    }

}
