using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemiga : MonoBehaviour
{
    private Animator animator;
    private SpriteRenderer sprite;
    private Rigidbody2D rb;

    private float velocidad = -8f;

    void Start()
    {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
    }

   void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Suelo")
        {
            rb.velocity = new Vector2(velocidad, 45);
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if(velocidad == -12f)
        {
            velocidad = 12f;
            sprite.flipX = true;
        }
        else
        {
            velocidad = -12f;
            sprite.flipX = false;
        }
    }
}
